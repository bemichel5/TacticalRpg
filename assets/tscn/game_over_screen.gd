extends CanvasLayer

var t : TacticsLevel
func _on_restart_pressed():
	get_tree().get_root().remove_child(get_tree().get_root().get_node(("Level")))
	if TCP._isServer:
		gamestate.begin_game()
	get_parent().remove_child(get_parent().get_node("GameOverScreen"))



func _on_quit_pressed():
	get_tree().quit()

func isRemoteWinner(isRemote):
	if isRemote:
		$PanelContainer/MarginContainer/Rows/WhoWin.text = "You lose ... I'm disapointed. "
	else:
		$PanelContainer/MarginContainer/Rows/WhoWin.text = "You win ... But do you deserve it?  "
