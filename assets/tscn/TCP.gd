extends Node
class_name TCP_handler

var TCP : TCPServer
var peer : StreamPeerTCP
const DEFAULT_PORT = 10567

var _isServer = false
var _isClient = false

var _isConnected = false

signal msg_received(data)
signal recv_move(data)
signal recv_change_turn(data)
signal recv_attack(data)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _isServer:
		if TCP.is_connection_available():
			print("maybe")
			peer = TCP.take_connection()
			if peer:
				print("yes")
				gamestate.register_player(peer.get_connected_host())
				_isConnected = true
				
	if _isConnected:
		recv()

func client(host):
	_isClient = true
	peer = StreamPeerTCP.new()
	peer.connect_to_host(host, DEFAULT_PORT)
	_isConnected = true
	
func server():
	_isServer = true
	TCP = TCPServer.new()
	TCP.listen(DEFAULT_PORT) # Port number StreamPeerTCP
	print("Server is running")

func isServer():
	return _isServer
	
func send(data):
	peer.put_data(data.to_ascii_buffer())

func recv():
	var data
	peer.poll()
	if  peer.get_available_bytes() and peer.get_status() == StreamPeerTCP.STATUS_CONNECTED:
		data = peer.get_string(peer.get_available_bytes())
		print("Response: ",data,"  STATUS: ",peer.get_status())
		msg_received.emit(data)
		handle_msg(data)
		
func handle_msg(data):
	#id:nom<param1><param2>
	match data[0]:
		#game start
		"1": gamestate.begin_game()
		#pawn move <nom_pion><tile>
		"2": recv_move.emit(data)
		"3": recv_change_turn.emit(data)
		"4": recv_attack.emit(data)
		
