extends Node3D
class_name TacticsLevel

var t_from = null
var t_to = null
var curr_t = null
var player : TacticsPlayerController = null
var enemy : TacticsPlayerController = null
var arena : TacticsArena
var camera : TacticsCamera
var ui_control : TacticsPlayerControllerUI

var started = false
func _ready():
	arena = $Arena
	camera = $TacticsCamera
	ui_control = $PlayerControllerUI
	TCP.recv_move.connect(_move_remote)
	TCP.recv_attack.connect(_attack_from_remote)

func _attack_from_remote(data):
	var params = data.split(":")
	var attacker = params[1]
	var attacked = params[2]
	var remote : TacticsPlayerController = null
	var current : TacticsPlayerController = null
	if TCP.isServer():
		current = player
		remote = enemy
	else:
		current = enemy
		remote = player
		
	var attacker_pawn = remote.get_node(attacker)
	var attacked_pawn = current.get_node(attacked)
	attacker_pawn.do_attack(attacked_pawn,0.1666)


func turn_handler(delta):
	if player.can_act() : player.act(delta)
	elif enemy.can_act(): enemy.act(delta)
	else:
		player.reset()
		enemy.reset()


func _physics_process(delta):
	if started:
		turn_handler(delta)

func create_server_player():
	player = $Player
	player.configure(arena, camera, ui_control, TCP.isServer())
	
func create_client_player():
	enemy = $Enemy
	enemy.configure(arena, camera, ui_control, !TCP.isServer())
	
func start():
	started = true
	
func _move_remote(data):
	var params = data.split(":")
	var pawn_s = params[1]
	var tile_s = params[2]
	var remote : TacticsPlayerController = null
	if TCP.isServer():
		remote = enemy
	else:
		remote = player
		
	var pawn = remote.get_node(pawn_s)
	remote.curr_pawn = pawn
	arena.link_tiles(pawn.get_tile(), pawn.jump_height, get_children())
	var Tiles = arena.get_node("Tiles")
	var tile
	for t in Tiles.get_children():
		if t.name == tile_s:
			tile = t
	
	
	
	remote.start_move(tile)
	
