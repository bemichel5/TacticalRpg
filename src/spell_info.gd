extends Resource

class_name Spell_Info

@export var Spell_name : String
@export_file var Visuals

@export var Mana_cost : int

@export var Damage : int
